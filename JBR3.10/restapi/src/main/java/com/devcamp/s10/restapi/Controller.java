package com.devcamp.s10.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> rainbowColorList(){
        ArrayList<String> colorList = new ArrayList<String>();
        colorList.add("red");
        colorList.add("orange");
        colorList.add("yellow");
        colorList.add("green");
        colorList.add("blue");
        colorList.add("indigo");
        colorList.add("violet");
        
        return colorList;
    }
}
